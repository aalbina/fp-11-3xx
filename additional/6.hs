sumN :: Int -> Int
sumN n = n * (n + 1) `div` 2

sqrSum :: Int -> Int
sqrSum 1 = 1
sqrSum n = n*n + sqrSum (n-1)

result :: Int -> Int
result 1 = 1
result n = (sumN n * sumN n) - (sqrSum n)

--- 25164150
