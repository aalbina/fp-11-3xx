mysum x = sum' 0 x
    where
        sum' a [] = a^a
        sum' a (x:xs) = a^a + sum' x xs

result = (mod (mysum [1 .. 1000]) 10000000000) - 1

--- 9110846700