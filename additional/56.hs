digitSum n = if n > 0
                then (mod n 10) + digitSum (div n 10)
                else div n 10

maxSum m i j = if i < 100
                then
                    if j < 100
                        then
                            if ((digitSum (i^j) ) > m)
                                then maxSum (digitSum (i^j)) i (j+1)
                                else maxSum m i (j+1)
                        else maxSum m (i+1) 1
                else m

--- maxSum 1 1 0
--- 972