countOfLetters :: Int -> Int
countOfLetters 0 = 0
countOfLetters 1 = 3
countOfLetters 2 = 3
countOfLetters 3 = 5
countOfLetters 4 = 4
countOfLetters 5 = 4
countOfLetters 6 = 3
countOfLetters 7 = 5
countOfLetters 8 = 5
countOfLetters 9 = 4
countOfLetters 10 = 3
countOfLetters 11 = 6
countOfLetters 12 = 6
countOfLetters 13 = 8
countOfLetters 14 = 8
countOfLetters 15 = 7
countOfLetters 16 = 7
countOfLetters 17 = 9
countOfLetters 18 = 8
countOfLetters 19 = 8
countOfLetters 20 = 6
countOfLetters 30 = 6
countOfLetters 40 = 5
countOfLetters 50 = 5
countOfLetters 60 = 5
countOfLetters 70 = 7
countOfLetters 80 = 6
countOfLetters 90 = 6
countOfLetters 100 = 7
countOfLetters 1000 = 8

countLetters :: Int -> Int
countLetters n
    | n == 100 = 10
    | n == 1000 = 11
    | n > 100 && n <= 999 && (n `mod` 100) == 0 =  countOfLetters (n `div` 100) + 7
    | n > 100 && n <= 999 && (n `mod` 100) > 0 && (n `mod` 100) >= 20 = countOfLetters (n `div` 100) + 7 + 3 + countOfLetters (((n `mod` 100) `div` 10) * 10) + countOfLetters (n `mod` 10)
    | n > 100 && n <= 999 && (n `mod` 100) > 0 && (n `mod` 100) < 20 = countOfLetters (n `div` 100) + 7 + 3 + countOfLetters (n `mod` 100)
    | n > 20 && n < 100 = countOfLetters (((n `mod` 100) `div` 10) * 10) + countOfLetters (n `mod` 10)
    | n <= 20 = countOfLetters n


result :: Int -> Int
result 1 = 3
result n = countLetters n + result (n-1)


--- 21124
