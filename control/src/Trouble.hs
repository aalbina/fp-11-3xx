{-==== Альмухаметова Альбина, 11-301 ====-}

module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}
rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket xs = error "Design rocket!"

{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Ord,Show)
data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}

checkCargo :: [Load a] -> [Load a]
checkCargo [] = []
checkCargo (x:xs) = case x of
                       Orbiter a -> [x] ++ checkCargo xs
                       Probe a -> checkCargo xs

orbiters :: [Spaceship a] -> [Load a]
orbiters [] = []
orbiters (x:xs) = case x of
                    Rocket i s -> orbiters s ++ orbiters xs
                    Cargo q -> checkCargo q ++ orbiters xs
--orbiters = error "Count orbiters!"

{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}

speaker :: Phrase -> String
speaker (Warp _) = "Kirk"
speaker (BeamUp _) = "Kirk"
speaker (IsDead _) = "McCoy"
speaker LiveLongAndProsper = "Spock"
speaker Fascinating = "Spock"

finalFrontier :: [Phrase] -> [String]
finalFrontier [] = []
finalFrontier (x:xs) = speaker x : (finalFrontier xs)
--finalFrontier = error "Space, the final frontier. These are the voyages of the Starship Enterprise. Its five-year mission: to explore strange new worlds, to seek out new life and new civilizations, to boldly go where no man has gone before."

