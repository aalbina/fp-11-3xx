-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown On = True
isKnown Off = True
isKnown _ = False

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа

eval :: Term -> Int
eval (Const x) = x
eval (Add x y) = (eval x) + (eval y)
eval (Sub x y) = (eval x) - (eval y)
eval (Mult x y) = (eval x) * (eval y)


-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify (Const x) = Const x
simplify (Mult x (Add y z)) = simplify (Add (Mult (simplify x) (simplify y)) (Mult (simplify x) (simplify z)))
simplify (Mult (Add x y) z) = simplify (Add (Mult (simplify x) (simplify z)) (Mult (simplify y) (simplify z)))
simplify (Mult x (Sub y z)) = simplify (Sub (Mult (simplify x) (simplify y)) (Mult (simplify x) (simplify z)))
simplify (Mult (Sub x y) z) = simplify (Sub (Mult (simplify x) (simplify z)) (Mult (simplify y) (simplify z)))
simplify (Add x y) = Add (simplify x) (simplify y)
simplify (Sub x y) = Sub (simplify x) (simplify y)
simplify (Mult x y) = Mult (simplify x) (simplify y)

